import 'package:flutter/material.dart';


void main() {
  runApp(Contact());
}

class Contact extends StatelessWidget {
  const Contact({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                    width: 450,
                    //Height constraint at Container widget level
                    height: 200,
                    child: Image.network("https://upload.wikimedia.org/wikipedia/th/2/27/Buu0001.jpeg",
                        errorBuilder: (context,url, error) => Icon(Icons.error)
                    )
                ),
                Container(
                  margin: EdgeInsets.only(top: 10, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildProfileButton(),
                      buildTableViewButton(),

                    ],
                  ),
                ),
                Divider(
                  color: Colors.amber,
                ),
                Container(
                  margin: EdgeInsets.only(top: 1, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildMessageButton(),
                      buildPetitionButton(),


                    ],
                  ),
                ),
                Divider(
                  color: Colors.amber,
                ),
                Container(
                  margin: EdgeInsets.only(top: 1, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildTableButton(),
                      buildRecentButton(),

                    ],
                  ),
                ),
                Divider(
                  color: Colors.amber,
                ),
                Container(
                  margin: EdgeInsets.only(top: 1, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildRelationsButton(),
                      buildCalendarButton(),

                    ],
                  ),
                ),
                // Container(
                //   height: 60,
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.start,
                //     children: <Widget>[
                //       Padding(
                //         padding: EdgeInsets.all(8.0),
                //         child: Text("ประกาศสำคัญ!!",
                //           style: TextStyle(color: Colors.pinkAccent, fontSize:30),
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
                // Container(
                //   height: 40,
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.start,
                //     children: <Widget>[
                //       Padding(
                //         padding: EdgeInsets.all(8.0),
                //         child: Text("กำหนดการชำระค่าบำรุงหอพักภาคฤดูร้อน "
                //             "ปีการศึกษา 2565",
                //           style: TextStyle(fontSize:15),
                //         ),
                //       ),
                //     ],
                //   ),
                // ),

                Container(
                    width: 450,
                    //Height constraint at Container widget level
                    height: 300,
                    child: Image.network("https://pbs.twimg.com/media/FG0EHlyVcAgzFxT?format=jpg&name=large",
                        errorBuilder: (context,url, error) => Icon(Icons.error)
                    )
                ),
                Divider(
                  color: Colors.yellow,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildProfileButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.location_history  ,
          color: Colors.blueGrey[300],
        ),
        onPressed: () {
          // Navigator.push(
          // context,
          //     MaterialPageRoute(builder: (context) => StudentProfile()),
          //    );
        },

      ),
      Text("บัญชีผู้ใช้งาน"),
    ],
  );
}

Widget buildCalendarButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.edit_calendar_rounded ,
          color: Colors.blueGrey[300],
        ),
        onPressed: () {},
      ),
      Text("ตรวจสอบวันจบ"
          "การศึกษา"),
    ],
  );
}

Widget buildTableButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.list_alt_outlined ,
          color: Colors.blueGrey[300],
        ),
        onPressed: () {},
      ),
      Text("ตารางเรียน/สอบ"),
    ],
  );
}

Widget buildMessageButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message ,
          color: Colors.blueGrey[300],
        ),
        onPressed: () {},
      ),
      Text("ภาระค่าใช้จ่ายทุน"),
    ],
  );
}

Widget buildRecentButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.recent_actors ,
          color: Colors.blueGrey[300],
        ),
        onPressed: () {},
      ),
      Text("หลักสูตรที่เปิดสอน"),
    ],
  );
}

Widget buildTableViewButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.view_sidebar_outlined  ,
          color: Colors.blueGrey[300],
        ),
        onPressed: () {},
      ),
      Text("ปฎิทินการศึกษา"),
    ],
  );
}

Widget buildPetitionButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
            Icons.attach_email_outlined  ,
            color: Colors.blueGrey[300]
        ),
        onPressed: () {},
      ),
      Text("คำร้อง/แบบฟอร์ม"),
    ],
  );
}

Widget buildRelationsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.language_sharp   ,
          color: Colors.blueGrey[300],
        ),
        onPressed: () {},
      ),
      Text("ประชาสัมพันธ์"),
    ],
  );
}