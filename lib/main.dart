import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'contact.dart';


void main() {
  runApp(const RegApp());
}

class RegApp extends StatelessWidget {
  const RegApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) =>
          MaterialApp(
            debugShowCheckedModeBanner: false,
            useInheritedMediaQuery: true,
            builder: DevicePreview.appBuilder,
            locale: DevicePreview.locale(context),
            title: 'Responsive and adaptive UI in Flutter',
            theme: ThemeData(
              primarySwatch: Colors.yellow,
            ),
            home: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                title: const Text('Registered BUU'),
              ),
              // drawer: Container(
              //   width: 250,
              //   color: Colors.red.shade200,
              //   // margin: EdgeInsets.only(top: 15, bottom: 8),
              //   child: Row(
              //    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //    children: <Widget>[
              //       buildTableViewButton(),
              //
              //     ],
              //    ),
              // ),
              drawer: Drawer(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    UserAccountsDrawerHeader(
                      accountName: Text('Pannaporn Somjitsakul'),
                      accountEmail: Text('62160285@go.buu.ac.th'),
                      currentAccountPicture: CircleAvatar(
                        child: Text("P"),
                        foregroundColor: Colors.white,
                        backgroundColor: Colors.purpleAccent,
                      ),
                      otherAccountsPictures: <Widget>[

                      ],
                    ),

                    ListTile(
                      leading: Icon(Icons.view_sidebar_outlined),
                      tileColor: Colors.grey[300],
                      trailing: Icon(Icons.more_vert),
                      title: Text('ปฎิทินการศึกษา'),
                      // onTap: () {
                      //   Navigator.push(
                      //     context,
                      //     MaterialPageRoute(builder: (context) => Profile()),
                      //   );
                      // },
                    ),
                    ListTile(
                      leading: Icon(Icons.list_alt_outlined ),
                      tileColor: Colors.grey[300],
                      trailing: Icon(Icons.more_vert),
                      title: Text('ตารางสอน/ตารางสอบ'),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.library_books_sharp ),
                      tileColor: Colors.grey[300],
                      trailing: Icon(Icons.more_vert),
                      title: Text('วิชาที่เปิดสอน'),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.edit_calendar_rounded),
                      tileColor: Colors.grey[300],
                      trailing: Icon(Icons.more_vert),
                      title: Text('สำเร็จการศึกษา'),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.location_history),
                      tileColor: Colors.grey[300],
                      trailing: Icon(Icons.more_vert),
                      title: Text('ประวัติการศึกษา'),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.message_outlined),
                      tileColor: Colors.grey[300],
                      trailing: Icon(Icons.more_vert),
                      title: Text('ผลการศึกษา'),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.sensor_door_outlined),
                      tileColor: Colors.grey[300],
                      trailing: Icon(Icons.more_vert),
                      title: Text('ออกจากระบบ'),
                      onTap: () {},
                    ),
                    Divider()

                    // Expanded(
                    //   child: Align(
                    //     alignment: Alignment.bottomLeft,
                    //     child: ListTile(
                    //       title: Text('ออกจากระบบ'),
                    //       onTap: () {},
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              ),
              body: const Contact(),
            ),
          ),
    );
  }
}